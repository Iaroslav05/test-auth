import { FC, useState } from "react";
import { Box, TextField, Typography } from "@mui/material";
import { useGoBack } from "../../hooks/useGoBack";

import Button from "../Button";
import { Link } from "react-router-dom";
import { useLogin } from "../../hooks/useLogin";

const LoginForm: FC = () => {
  const goBack = useGoBack();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const { loginUsers } = useLogin({ email, password });

const handleLoginUser = () => {
loginUsers();
  setEmail("");
  setPassword("");
};


  return (
    <>
      <div>
        <Typography variant="h4" align="center" m={4}>
          Login Form
        </Typography>
      </div>
      <Box
        component="form"
        sx={{
          "& .MuiTextField-root": { m: 1, width: "50%" },
        }}
        noValidate
        autoComplete="off"
      >
        {/* {!isPassword && (
          <Typography color="red" variant="h6" align="center" m={4}>
            Password not match
          </Typography>
        )} */}

        <div style={{ display: "flex" }}>
          <TextField
            label="Enter your email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
          <TextField
            type="password"
            label="Enter your password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
        </div>
      </Box>
      <div style={{ display: "flex", justifyContent: "space-around" }}>
        <Link style={{ textDecoration: "none" }} to={"/"}>
          <Button onClick={handleLoginUser} buttonText="Log in" />
        </Link>
		  <Link style={{ textDecoration: "none" }} to={"/registartion"}>
        <Button onClick={goBack} buttonText="Create Account" color="warning" />
		  </Link>
        <Button onClick={goBack} buttonText="Cancel" color="error" />
      </div>
    </>
  );
};

export default LoginForm;
