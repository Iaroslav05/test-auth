import domain from "../config/API";
import inMemoryToken from "../service/inMemoryToken";

export const useLogin = ({
  email,
  password,
}: {
  email: string;
  password: string;
}) => {

  const loginUsers = async () => {
    try {
      const response = await fetch(`${domain}/login`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",

        },
        body: JSON.stringify({
          email,
          password,
        }),
      });
      if (!response.ok) {
        throw new Error(`Error Api: status ${response.status}`);
      }
      const { token } = await response.json();
		inMemoryToken.setToken(token);
		localStorage.setItem("token", token);
		

    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };
  return { loginUsers };
}